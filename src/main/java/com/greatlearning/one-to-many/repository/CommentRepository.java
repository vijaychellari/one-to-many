package com.wipro.one2many.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wipro.one2many.entity.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long>{

}
