package com.wipro.one2many;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.wipro.one2many.entity.Comment;
import com.wipro.one2many.entity.Post;
import com.wipro.one2many.repository.PostRepository;

@SpringBootApplication
public class One2manyApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(One2manyApplication.class, args);
	}
	
	@Autowired
	private PostRepository postRepository;
	
	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		Post post = new Post("one to many mapping using JPA and hibernate", "one to many mapping using JPA and hibernate");

        Comment comment1 = new Comment("Very useful");
        Comment comment2 = new Comment("informative");
        Comment comment3 = new Comment("Great post");

        post.getComments().add(comment1);
        post.getComments().add(comment2);
        post.getComments().add(comment3);

        this.postRepository.save(post);
		
	}

}
